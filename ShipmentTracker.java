package day11;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;


//first draft
@SuppressWarnings("unused")
public class ShipmentTracker {
	
	int thours;   // delivery time
	LocalDate ld; // shipping date
	LocalTime lt;					// shipping time hr-min-sec
	
	LocalTime end=LocalTime.of(18,0,0);
	LocalTime st=LocalTime.of(06,0,0);
	
	ArrayList<Holidays> l=new ArrayList<>();
	
	int ini=0;  //initial hrs
	int perday=12;
	int copy=perday;
	
	public ShipmentTracker(LocalDate ld, LocalTime lt, int thours)
	{
		this.ld=ld;
		this.lt=lt;
		this.thours=thours;
	}

	public String calculateDays()
	{
		
		int rem=thours;
		if(lt.compareTo(st) <= 0)
		{  perday=copy;
		} 
		else if(lt.compareTo(end) >=0)
		{
			perday=copy;
			ld.plusDays(1);
		}
		else
		{
			perday= end.getHour()-lt.getHour();
		}
		
    	int mints=lt.getMinute();
		
		l.addAll(Arrays.asList(Holidays.values()));
		
		while(ini <thours)
		{
			String w=ld.getDayOfWeek().toString();
			String holi=ld.getMonth().toString() + Integer.toString(ld.getDayOfMonth());
			
			try
			{
			if(l.contains(Holidays.valueOf(holi)))
			{
				ld=ld.plusDays(1);
				continue;
			}
			}
			catch (Exception e) {		}
			
				DayWeek dow=DayWeek.valueOf(w);
				if(rem<=12)
				{
					ini+=  rem;
				}
				else
				{
				ini+=  dow.work(perday);
				}
				perday=copy;
				if(ini!=thours) {ld=ld.plusDays(1);}
				else { break;}
				rem=thours-ini;
			
			
		}
		
		st= st.plusHours(rem);
		
		if(st.plusMinutes(mints).compareTo(end) >0)
		{
			Duration diff= Duration.between(end,st.plusMinutes(mints));
			ld=ld.plusDays(1);
			st=LocalTime.of(06,0).plus(diff); 
		}
		else
		{
			st=st.plusMinutes(mints);
		}
		
		
		String msg="delivery date"+": "+ld+" "+ld.getDayOfWeek()+"  time: "+st;
		
		System.out.println(msg);
		return msg;
		
		
	}
	public String getTimeZone()
		
	{	LocalDateTime ldt=LocalDateTime.of(ld,st);
		return "timezone UTC: "+ ldt.atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime();
	}

		
	}
	enum DayWeek
	{   
		MONDAY {
			@Override
			int work(int n) {
				return n;
			}
		},TUESDAY {
			@Override
			int work(int n) {
				 return n;
			}
		},WEDNESDAY
		{
			@Override
			int work(int n) {
				 return n;
			}
		},
		THURSDAY
		{

			@Override
			int work(int n) {
				return n;
			}
		
		},
		FRIDAY
		{

			@Override
			int work(int n) {
				return n;
			}
		
		},
		SATURDAY
		{

			@Override
			int work(int n) {
				return 0;
			}
			
		},
		SUNDAY
		{

			@Override
			int work(int n) {
				return 0;
			}
			
		};
		
		abstract int work(int n);
	}
	enum Holidays
	{
		JANUARY1,JANUARY26,AUGUST15;
	}


