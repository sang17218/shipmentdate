package day11;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.*;
import org.junit.Test;



public class ShipmentDeliveryDate {
	
	ShipmentTracker obj;   // date -  time - thours
	
	@Test
	public void holidayCheck()  // holidays
	{
		obj=new ShipmentTracker(LocalDate.of(2020, 8, 14), LocalTime.of(12, 15, 30), 13);
		assertEquals("delivery date: 2020-08-16 SUNDAY  time: 13:15",obj.calculateDays()); 
		//  delivery date: 2020-08-16 SUNDAY  time: 13:00 
	
	}
	
	@Test
	public void weekendCheck()  // starts on weekend
	{
		obj=new ShipmentTracker(LocalDate.of(2020, 9, 26), LocalTime.of(8, 15, 30), 52);
		assertEquals("delivery date: 2020-10-02 FRIDAY  time: 10:15",obj.calculateDays());
		// delivery date: 2020-10-02 FRIDAY  time: 10:00 
		
	}
	
	@Test
	public void weekendCheck1()  // encounters  weekend
	{
		obj=new ShipmentTracker(LocalDate.of(2020, 9, 23), LocalTime.of(8, 15, 30), 49);
		assertEquals("delivery date: 2020-09-29 TUESDAY  time: 09:15",obj.calculateDays());
		// delivery date: 2020-09-29 TUESDAY  time: 09:00 
	}
	
	@Test
	public void normalCheck()   // no interruption  with perday constraints
	{
		obj=new ShipmentTracker(LocalDate.of(2020, 9, 21), LocalTime.of(07, 45, 30), 48);
		assertEquals("delivery date: 2020-09-25 FRIDAY  time: 07:45",obj.calculateDays());
		// delivery date: 2020-09-25 FRIDAY  time: 07:00 
	}
	
	@Test
	public void y2kCheck()   // y2k with weekends
	{
		obj=new ShipmentTracker(LocalDate.of(2000, 1, 1), LocalTime.of(07, 5, 30), 30);
		assertEquals("delivery date: 2000-01-05 WEDNESDAY  time: 12:05",obj.calculateDays());
		// delivery date: 2000-01-05 WEDNESDAY  time: 12:00 
	}
	
	@Test
	public void leapCheck()   // leap yr chck
	{
		obj=new ShipmentTracker(LocalDate.of(2008, 02, 28), LocalTime.of(06, 5, 30), 14);
		assertEquals("delivery date: 2008-02-29 FRIDAY  time: 08:05",obj.calculateDays());
		// delivery date: 2008-02-29 FRIDAY  time: 08:00 
	}
	
	@Test
	public void timeZone()
	{
		obj=new ShipmentTracker(LocalDate.of(2008, 02, 28), LocalTime.of(06, 5, 30), 14);
		obj.calculateDays();
		assertEquals("timezone UTC: 2008-02-29T02:35",obj.getTimeZone());
	}
//	

}
